from django.test import TestCase, Client
from django.urls import resolve

from .models import Calories
from .views import form_cal, inputCal
from .forms import calories

class test_calories_calculator(TestCase):
    def test_response_page(self):
        response = Client().get('/calories-cal/')
        self.assertEqual(response.status_code, 200)

    def test_title_page(self):
        response = Client().get('/calories-cal/')
        title_body_html = response.content.decode('utf8')
        self.assertIn("Calories Calculator", title_body_html)

    def test_views_forms(self):
        found = resolve('/calories-cal/')
        self.assertEqual(found.func, form_cal)
    
    def test_calculate_button(self):
        response = Client().get('/calories-cal/')
        body_button = response.content.decode('utf8')
        self.assertIn('<button type="submit" style="background-image: linear-gradient(#EC4057, #F57434); color: #ffffff; border-radius: 14px;" class="btn btn" id="bton">Calculate</button>', body_button)

    def test_views_res(self):
        found = resolve('/calories-cal/res/')
        self.assertEqual(found.func, inputCal)


