from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Calories
from .forms import calories

def form_cal(request):
    form_calc = calories()
    data = Calories.objects.all()
    response = {
        'form_calc': form_calc,
        'data': data,
    }
    return render(request, 'Calories_Calc/calorIn.html', response)

def inputCal(request):
    if request.method == 'POST':
        form = calories(request.POST)
        response_data = {}
        form_calories = calories()
        data = Calories.objects.all()
        response = {
           'form_calories': form_calories,
           'data': data,
        }
        
        if form.is_valid():
            response_data['age'] = int(request.POST['age'])
            response_data['gender'] = request.POST['gender']
            response_data['height'] = int(request.POST['height'])
            response_data['weight'] = int(request.POST['weight'])
            response_data['activity'] = request.POST['activity']

            data_respon = Calories(age=response_data['age'],
                                    gender=response_data['gender'],
                                    height=response_data['height'],
                                    weight=response_data['weight'],
                                    activity=response_data['activity'],
                                    )
            data_respon.save()

            if response_data['gender'] == 'Male':
                result = (66 + (13.7 * response_data['weight']) + (5 * response_data['height']) - (6.8 * response_data['age']))
                if response_data['activity'] == 'Sedentary: little or no execersice':
                    resultAct = result * 1.2
                    gainRes = resultAct * 1.23
                    losRes = resultAct * 0.75
                    response = {
                    'form_calories': form_calories,
                    'data': data,
                    'resultAct' : resultAct,
                    'gainRes' : gainRes,
                    'losRes' : losRes,
                    }
                    return render(request, 'Calories_Calc/calories-cal.html', response)
                elif response_data['activity'] == 'Light: exercise 1-3 times/week':
                    resultAct = result * 1.4
                    gainRes = resultAct * 1.23
                    losRes = resultAct * 0.75
                    response = {
                    'form_calories': form_calories,
                    'data': data,
                    'resultAct' : resultAct,
                    'gainRes' : gainRes,
                    'losRes' : losRes,
                    }
                    return render(request, 'Calories_Calc/calories-cal.html', response)
                elif response_data['activity'] == 'Moderate: exercise 4-5 times/week':
                    resultAct = result * 1.5
                    gainRes = resultAct * 1.23
                    losRes = resultAct * 0.75
                    response = {
                    'form_calories': form_calories,
                    'data': data,
                    'resultAct' : resultAct,
                    'gainRes' : gainRes,
                    'losRes' : losRes,
                    }
                    return render(request, 'Calories_Calc/calories-cal.html', response)
                elif response_data['activity'] == 'Active: daily exercise or intense exercise 3-4 times/week':
                    resultAct = result * 1.6
                    gainRes = resultAct * 1.23
                    losRes = resultAct * 0.75
                    response = {
                    'form_calories': form_calories,
                    'data': data,
                    'resultAct' : resultAct,
                    'gainRes' : gainRes,
                    'losRes' : losRes,
                    }
                    return render(request, 'Calories_Calc/calories-cal.html', response)
                elif response_data['activity'] == 'Very Active: intense exercise 6-7 times/week':
                    resultAct = result * 1.7
                    gainRes = resultAct * 1.23
                    losRes = resultAct * 0.75
                    response = {
                    'form_calories': form_calories,
                    'data': data,
                    'resultAct' : resultAct,
                    'gainRes' : gainRes,
                    'losRes' : losRes,
                    }
                    return render(request, 'Calories_Calc/calories-cal.html', response)
                elif response_data['activity'] == 'Extra Active: very intense exercise daily, or physical job':
                    resultAct = result * 1.9
                    gainRes = resultAct * 1.23
                    losRes = resultAct * 0.75
                    response = {
                    'form_calories': form_calories,
                    'data': data,
                    'resultAct' : resultAct,
                    'gainRes' : gainRes,
                    'losRes' : losRes,
                    }
                    return render(request, 'Calories_Calc/calories-cal.html', response)
                
            elif response_data['gender'] == 'Female':
                result = (665 + (9.6 * response_data['weight']) + (1.8 * response_data['height']) - (4.7 * response_data['age']))
                if response_data['activity'] == 'Sedentary: little or no execersice':
                    resultAct = result * 1.2
                    gainRes = resultAct * 1.23
                    losRes = resultAct * 0.75
                    response = {
                    'form_calories': form_calories,
                    'data': data,
                    'resultAct' : resultAct,
                    'gainRes' : gainRes,
                    'losRes' : losRes,
                    }
                    return render(request, 'Calories_Calc/calories-cal.html', response)
                elif response_data['activity'] == 'Light: exercise 1-3 times/week':
                    resultAct = result * 1.4
                    gainRes = resultAct * 1.23
                    losRes = resultAct * 0.75
                    response = {
                    'form_calories': form_calories,
                    'data': data,
                    'resultAct' : resultAct,
                    'gainRes' : gainRes,
                    'losRes' : losRes,
                    }
                    return render(request, 'Calories_Calc/calories-cal.html', response)
                elif response_data['activity'] == 'Moderate: exercise 4-5 times/week':
                    resultAct = result * 1.5
                    gainRes = resultAct * 1.23
                    losRes = resultAct * 0.75
                    response = {
                    'form_calories': form_calories,
                    'data': data,
                    'resultAct' : resultAct,
                    'gainRes' : gainRes,
                    'losRes' : losRes,
                    }
                    return render(request, 'Calories_Calc/calories-cal.html', response)
                elif response_data['activity'] == 'Active: daily exercise or intense exercise 3-4 times/week':
                    resultAct = result * 1.6
                    gainRes = resultAct * 1.23
                    losRes = resultAct * 0.75
                    response = {
                    'form_calories': form_calories,
                    'data': data,
                    'resultAct' : resultAct,
                    'gainRes' : gainRes,
                    'losRes' : losRes,
                    }
                    return render(request, 'Calories_Calc/calories-cal.html', response)
                elif response_data['activity'] == 'Very Active: intense exercise 6-7 times/week':
                    resultAct = result * 1.7
                    gainRes = resultAct * 1.23
                    losRes = resultAct * 0.75
                    response = {
                    'form_calories': form_calories,
                    'data': data,
                    'resultAct' : resultAct,
                    'gainRes' : gainRes,
                    'losRes' : losRes,
                    }
                    return render(request, 'Calories_Calc/calories-cal.html', response)
                elif response_data['activity'] == 'Extra Active: very intense exercise daily, or physical job':
                    resultAct = result * 1.9
                    gainRes = resultAct * 1.23
                    losRes = resultAct * 0.75
                    response = {
                    'form_calories': form_calories,
                    'data': data,
                    'resultAct' : resultAct,
                    'gainRes' : gainRes,
                    'losRes' : losRes,
                    }
                    return render(request, 'Calories_Calc/calories-cal.html', response)
            
            return render(request, 'Calories_Calc/calories-cal.html', response)
        else:
            print(form.errors)
            return redirect('Calories_Calc/calories-cal.html')

    else:
        return redirect('Calories_Calc/calories-cal.html')


