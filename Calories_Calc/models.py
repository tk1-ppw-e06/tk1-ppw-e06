from django.db import models

# Create your models here.

class Calories(models.Model):
    # GENDERS = (
    #     ('Male', 'Male'),
    #     ('Female', 'Female'),
    # )

    # ACTIVITY = (
    #     ('Sedentary: little or no execersice', 'Sedentary: little or no execersice'),
    #     ('Light: exercise 1-3 times/week', 'Light: exercise 1-3 times/week'),
    #     ('Moderate: exercise 4-5 times/week', 'Moderate: exercise 4-5 times/week'),
    #     ('Active: daily exercise or intense exercise 3-4 times/week', 'Active: daily exercise or intense exercise 3-4 times/week'),
    #     ('Very Active: intense exercise 6-7 times/week', 'Very Active: intense exercise 6-7 times/week'),
    #     ('Extra Active: very intense exercise daily, or physical job', 'Extra Active: very intense exercise daily, or physical job')
    # )

    age = models.IntegerField()
    gender = models.CharField(max_length=100,) #dropdown
    weight = models.IntegerField()
    height = models.IntegerField()
    activity = models.CharField(max_length=100,) #drop down

    def __str__(self):
        return self.age

