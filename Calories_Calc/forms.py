from django import forms

class calories(forms.Form):
    error_messages = {
        'required': 'This field is required.'
    }
    attrs = {
        'class': 'form-control',
        'style': 'border-radius: 10px'
    }
    GENDERS = [
        ('Male', 'Male'),
        ('Female', 'Female'),
    ]
    ACTIVITY = [
        ('Sedentary: little or no execersice', 'Sedentary: little or no execersice'),
        ('Light: exercise 1-3 times/week', 'Light: exercise 1-3 times/week'),
        ('Moderate: exercise 4-5 times/week', 'Moderate: exercise 4-5 times/week'),
        ('Active: daily exercise or intense exercise 3-4 times/week', 'Active: daily exercise or intense exercise 3-4 times/week'),
        ('Very Active: intense exercise 6-7 times/week', 'Very Active: intense exercise 6-7 times/week'),
        ('Extra Active: very intense exercise daily, or physical job', 'Extra Active: very intense exercise daily, or physical job')
    ]

    age = forms.IntegerField(widget=forms.TextInput(attrs=attrs), label='Age ', required=True)
    gender = forms.CharField(label='Gender ', widget=forms.Select(choices=GENDERS, attrs={'style': 'width:100%; height: 40px; border-radius: 10px' }))
    weight = forms.IntegerField(widget=forms.TextInput(attrs=attrs), label='Weight (kg) ', required=True)
    height = forms.IntegerField(widget=forms.TextInput(attrs=attrs), label='Height (cm) ', required=True)
    activity = forms.CharField(label='Activity ', widget=forms.Select(choices=ACTIVITY, attrs={'style': 'width:100%; height: 40px; border-radius: 10px'}))

