# Tugas Kelompok 1 (E06)

[![Test and Deploy][actions-badge]][commits-gh]
[![pipeline status][pipeline-badge]][commits-gl]
[![coverage report][coverage-badge]][commits-gl]

Repositori ini berisi sebuah templat untuk membuat proyek Django yang siap
di-*deploy* ke Heroku melalui GitHub Actions atau GitLab CI.


# Nama-nama anggota

- Muhammad Farhan Ramadhan     - 1906306155
- Fiorentine Phillips          - 1906298916 
- Faizah                       - 1906298885
- Rizma Chaerani Heidy Putri   - 1906299111
- Mochammad Naufal Rizki       - 1906299010

# Link Herokuapp 

https://tk1-e06.herokuapp.com/

# Deskripsi TK
Aplikasi yang kelompok kami buat adalah HealthLab. HealthLab adalah aplikasi penjaga kesehatan dan kebugaran yang membantu orang-orang pada kegiatan sehari-harinya. Pada masa pandemi Covid-19 ini, yang mengharuskan masyarakat untuk tetap didalam rumah dapat menyebabkan kurangnya aktivitas fisik dan makan makanan yang sehat untuk menjaga imunitas tubuh, oleh sebab itu HealthLab akan membantu menjadi sumber informasi dan sarana untuk masyarakat menjaga kesehatannya.

Di HealthLab ada beberapa sarana untuk membantu orang-orang dalam mengukur tingkat kesehatannya, seperti BMR(Basal Metabolic Rate) calculator, BMI(Body Mass Index)calculator, dan Calories calculator. Lalu dengan adanya artikel-artikel dan To-do List, dapat menjadi suatu sarana informasi terhadap bagaimana cara hidup sehat dan membantu menerapkan kehidupan sehat tersebut.



# Fitur-fitur

- **BMR** 
    User dapat menghitung estimasi berapa besar BMR (Basal Metabolic Rate).
- **BMI**
    User dapat menghitung estimasi berapa besar BMI (Body Mass Index).
- **Calories**
    User dapat menghitung estimasi berapa besar Calories/week yang dibutuhkan.
- **Article** 
    User dapat melihat berbagai artikel-artikel tentang kesehatan dan dapat membuat artikel kesehatan yang dapat bermanfaat.
- **To-Do List**
    User dapat membuat list-list bagaimana cara untuk menerapkan kehidupan yang sehat pada sehari-hari.

# Pembagian Tugas

- **BMR** Fitur ini dikerjakan oleh Fiorentine Phillips
- **BMI** Fitur ini dikerjakan oleh Rizma Chaerani Haidy Putri
- **Calories** Fitur ini dikerjakan oleh Farhan Ramadhan
- **Article** Fitur ini dikerjakan oleh Naufal Rizki
- **To-Do List** Fitur ini dikerjakan oleh Faizah

[actions-badge]: https://github.com/laymonage/django-template-heroku/workflows/Test%20and%20Deploy/badge.svg
[commits-gh]: https://github.com/laymonage/django-template-heroku/commits/master
[pipeline-badge]: https://gitlab.com/laymonage/django-template-heroku/badges/master/pipeline.svg
[coverage-badge]: https://gitlab.com/laymonage/django-template-heroku/badges/master/coverage.svg
[commits-gl]: https://gitlab.com/laymonage/django-template-heroku/-/commits/master
