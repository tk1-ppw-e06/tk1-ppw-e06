from django.shortcuts import render, redirect
from .forms import FormBMI
from .models import BMI

# Create your views here.
def BMIHome(request):
    if request.method == 'POST':
        form = FormBMI(request.POST)
        response_data = {}
        response_data['height'] = request.POST['height']
        response_data['weight'] = request.POST['weight']
        result = int(response_data['weight']) / ((int(response_data['height'])/100)**2)
        # data = BMI(height=response_data['height'], weight=response_data['weight'])
        if form.is_valid():
            form.save()
            return render(request, 'BMIResult.html', {'result' : result, 'form' : form})

    form = FormBMI()
    return render(request, 'BMIhome.html', {'form' : form})

def BMIResult(request):
    return render(request, 'BMIResult.html')