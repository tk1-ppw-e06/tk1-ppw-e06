# Generated by Django 3.1.2 on 2020-11-15 17:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bmi', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bmi',
            name='height',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='bmi',
            name='weight',
            field=models.IntegerField(default=0),
        ),
    ]
