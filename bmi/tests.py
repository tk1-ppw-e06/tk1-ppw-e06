from django.test import TestCase, Client
from django.urls import resolve
from .models import BMI 
from . import views

# Create your tests here.
class BMIUnitTest(TestCase):
    def test_url_bmi_exist(self):
        response = Client().get('/bmi/')
        self.assertEqual(response.status_code, 200)

    def test_template_bmi_used(self):
        response = Client().get('/bmi/')
        self.assertTemplateUsed(response, 'BMIhome.html')

    def test_model_can_create_new_data(self):
        data = BMI.objects.create(height='1', weight='1')
        test_data = BMI.objects.all().count()
        self.assertEqual(test_data, 1)
    
    def test_app_title(self):
        response = Client().get('/bmi/')
        page_Content = response.content.decode('utf8')
        self.assertIn("BMI Calculator", page_Content)
    
    def test_calculate_button_exist(self):
        response = Client().get('/bmi/')
        page_content = response.content.decode('utf8')
        self.assertIn('<button type="submit" class="btn btn-primary mt-3" style="background-image: linear-gradient(#EC4057, #F57434);">Calculate</button>', page_content)

    def test_view_forms(self):
        found = resolve('/bmi/')
        self.assertEqual(found.func, views.BMIHome)