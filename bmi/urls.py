from django.urls import include, path

from . import views

app_name = 'bmi'

urlpatterns = [
    path('', views.BMIHome, name='BMIHome'),
]