from django.urls import path

from . import views

app_name = 'articles'

urlpatterns = [
    path('', views.listArticles, name='list-articles'),
    path('create-article', views.createArticle, name='create-article'),
    path('<int:id>', views.articless, name='article'),
]