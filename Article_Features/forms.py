from django import forms
from .models import Articles

class article(forms.ModelForm):
    class Meta:
        model = Articles
        fields = [
            'tittle',
            'author',
            'description',
            'image',
        ]

        labels = {
            'tittle': 'Tittle',
            'author': 'Author',
            'description' : 'Description',
            'image' : 'Image'
        }

        widgets = {
            'tittle' : forms.TextInput(attrs= {'class' : 'form-control', 'style' : 'background-color: #E4E4E4;', 'placeholder' : 'ex: How to increase...',}),
            'author' : forms.TextInput(attrs= {'class' : 'form-control', 'style' : 'background-color: #E4E4E4;', 'placeholder' : 'ex: Sonya Anastasia',}),
            'description' : forms.Textarea(attrs= {'class' : 'form-control',}),
            'image' : forms.FileInput(attrs= {'class' : 'form-contol-file',}),
        }
