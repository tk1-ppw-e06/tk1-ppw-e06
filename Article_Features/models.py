from django.db import models
from cloudinary.models import CloudinaryField
from ckeditor.fields import RichTextField

import datetime
# Create your models here.

class Articles(models.Model):
    tittle = models.CharField(max_length=100)
    author = models.CharField(max_length=50)
    description = RichTextField(blank=True, null=True)
    image = CloudinaryField('image')
    create = models.DateTimeField(auto_now_add=True)
    update = models.DateTimeField(auto_now=True)


    def __str__(self):
        return '{}'.format(self.tittle)