from django.shortcuts import render, redirect

# Create your views here.

from django import forms
from django.http import HttpResponse

from cloudinary.forms import cl_init_js_callbacks
from .models import Articles
from .forms import article

def createArticle(request):
  context = dict(backend_form = article)
  if request.method == 'POST':
    form = article(request.POST, request.FILES)
    context['posted'] = form.instance
    if form.is_valid():
        form.save()
        return redirect('articles:list-articles')
  return render(request, 'Article_Features/create-article.html', context)

def listArticles(request):
    articles = Articles.objects.all()
    context = {'articles' : articles}
    return render(request, 'Article_Features/list-articles.html', context)

def articless(request, id):
    arc = Articles.objects.get(id=id)
    context = {'article' : arc}
    return render(request, 'Article_Features/article.html', context)
