from django.test import TestCase, Client
from django.urls import resolve

from .models import Articles
from .views import listArticles, createArticle
from .forms import article

# Create your tests here.

class Test(TestCase):
    def test_model_article(self):
        Articles.objects.create(tittle = "AIUEO", author = "Asep", description = "PPW is Fun")
        count = Articles.objects.all().count()
        self.assertEqual(count, 1)
        test = Articles.objects.get(tittle="AIUEO")
        self.assertEqual(str(test), "AIUEO")

    def test_url_list_articles(self):
        response = Client().get('/articles/')
        self.assertEqual(response.status_code, 200)

    def test_template_list_articles(self):
        response = Client().get('/articles/')
        self.assertTemplateUsed(response, 'Article_Features/list-articles.html')

    def test_index_func_list_articles(self):
        found = resolve('/articles/')
        self.assertEqual(found.func, listArticles)

    def test_url_create_article(self):
        response = Client().get('/articles/create-article')
        self.assertEqual(response.status_code, 200)

    def test_template_create_article(self):
        response = Client().get('/articles/create-article')
        self.assertTemplateUsed(response, 'Article_Features/create-article.html')

    def test_index_func_create_article(self):
        found = resolve('/articles/create-article')
        self.assertEqual(found.func, createArticle)

    def test_form_crate_article(self):
        response = self.client.post('/articles/create-article', data={'tittle': 'AIUEO', 'author': 'Asep'})

        self.assertEqual(response.status_code, 200)

    def test_view_list_article(self):
        response = Client().get('/articles/')
        isi_html_kembalian = response.content.decode('utf8')
        self.assertIn('Health Articles', isi_html_kembalian)

    def test_view_create_article(self):
        response = Client().get('/articles/create-article')
        isi_html_kembalian = response.content.decode('utf8')
        self.assertIn('Create Article', isi_html_kembalian)
        self.assertIn('Tittle', isi_html_kembalian)
        self.assertIn('Author', isi_html_kembalian)
        self.assertIn('Description', isi_html_kembalian)