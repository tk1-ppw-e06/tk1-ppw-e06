from django import forms

class bmrcal(forms.Form):
    error_messages = {
        'required': 'This field is required.'
    }
    attrs = {
        'class': 'form-control',
        'style': 'border-radius: 10px'
    }
    GENDER_CHOICES= [
    ('male', 'Male'),
    ('female', 'Female'),
    ]
    age = forms.IntegerField(widget=forms.TextInput(attrs=attrs), label='Age ', required=True)
    gender = forms.CharField(label='Gender ', widget=forms.Select(choices=GENDER_CHOICES, attrs={'style': 'width: 100%; height: 40px; border-radius: 10px'}))
    height = forms.IntegerField(widget=forms.TextInput(attrs=attrs), label='Height (cm) ', required=True)
    weight = forms.IntegerField(widget=forms.TextInput(attrs=attrs), label='Weight (kg) ', required=True)
    