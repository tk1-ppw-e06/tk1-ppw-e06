from django.test import TestCase, Client
from django.urls import resolve

from .models import BMR
from . import views


# Create your tests here.

class UnitTestBMRCalculator(TestCase):
    def test_response_page(self):
        response = Client().get('/bmrcalculator/')
        self.assertEqual(response.status_code, 200)

    def test_title_body(self):
        response = Client().get('/bmrcalculator/')
        title_body_html = response.content.decode('utf8')
        self.assertIn("BMR Calculator", title_body_html)

    def test_calculate_button(self):
        response = Client().get('/bmrcalculator/')
        body_button = response.content.decode('utf8')
        self.assertIn('<button type="submit"  class="calculate btn btn" id="bton">Calculate</button>', body_button)

    def test_bmrcalculator_template(self):
        response = Client().get('/bmrcalculator/')
        self.assertTemplateUsed(response, 'bmrcalculator.html')

    def test_views_forms(self):
        found = resolve('/bmrcalculator/')
        self.assertEqual(found.func, views.form_bmr)

    def test_views_post(self):
        found = resolve('/bmrcalculator/post/')
        self.assertEqual(found.func, views.post_bmrcal)

       
        


    
