from django.http import HttpResponse
from django.shortcuts import render, redirect

from bmrcalculator.models import BMR
from .forms import bmrcal


# Create your views here.

def post_bmrcal(request):
    if request.method == 'POST':
        form = bmrcal(request.POST)
        response_data = {}
        form_bmrcal = bmrcal()
        data = BMR.objects.all()
        
        if form.is_valid():
            response_data['age'] = int(request.POST['age'])
            response_data['gender'] = request.POST['gender']
            response_data['height'] = int(request.POST['height'])
            response_data['weight'] = int(request.POST['weight'])

            data_respon = BMR(age=response_data['age'],
                                    gender=response_data['gender'],
                                    height=response_data['height'],
                                    weight=response_data['weight'],
                                    )
            data_respon.save()

            if response_data['gender'] == 'male':
                result = (66 + (13.7 * response_data['weight']) + (5 * response_data['height']) - (6.8 * response_data['age']))
                response = {
                    'form_bmrcal': form_bmrcal,
                    'data': data,
                    'result' : result,
                }
                return render(request, 'bmrresult.html', response)
            elif response_data['gender'] == 'female':
                result = (665 + (9.6 * response_data['weight']) + (1.8 * response_data['height']) - (4.7 * response_data['age']))
                response = {
                    'form_bmrcal': form_bmrcal,
                    'data': data,
                    'result' : result,
                }
                return render(request, 'bmrresult.html', response)

        else:
            print(form.errors)
            return redirect('/bmrcalculator')

    else:
        return redirect('/bmrcalculator')



def form_bmr(request):
    form_bmrcal = bmrcal()
    data = BMR.objects.all()
    response = {
        'form_bmrcal': form_bmrcal,
        'data': data,
    }
    return render(request, 'bmrcalculator.html', response)
    







