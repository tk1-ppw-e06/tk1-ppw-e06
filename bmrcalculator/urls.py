from django.urls import path
from . import views

urlpatterns = [
    path('', views.form_bmr),
    path('post/', views.post_bmrcal),
]
