"""tk1_e06 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('main.urls')),
    path('bmi/', include('bmi.urls')),
    path('calories-cal/', include('Calories_Calc.urls')),
    path('to-do-list/', include('ToDoList_Features.urls', namespace='to-do-list')),
    path('articles/', include('Article_Features.urls', namespace='articles')),
    path('bmrcalculator/', include('bmrcalculator.urls'))
]
