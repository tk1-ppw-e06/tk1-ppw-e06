from django.apps import AppConfig


class TodolistFeaturesConfig(AppConfig):
    name = 'ToDoList_Features'
