from django.shortcuts import render, redirect

from django import forms
from django.http import HttpResponse

from .models import ToDoList
from .forms import ToDoForm

# Create your views here.
def createToDo(request):
    if request.method == 'POST':
        form = ToDoForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect('to-do-list:create-todo')
    
    todos = ToDoList.objects.all()
    print("todos")
    form = ToDoForm()

    context = {'todos':todos, 'form':form}
    return render(request, 'ToDoList_Features/create-todo.html', context)

def editToDo(request, pk):
    todo = ToDoList.objects.get(id=pk)
    form = ToDoForm(instance=todo)

    if request.method == 'POST':
        form = ToDoForm(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            return redirect('to-do-list:create-todo')

    context = {'form':form}
    return render(request, 'ToDoList_Features/edit-todo.html/', context)

def deleteToDo(request, pk):
    todo = ToDoList.objects.get(id=pk)

    if request.method == 'POST':
        todo.delete()
        return redirect('to-do-list:create-todo')

    context = {'todo':todo}
    return render(request, 'ToDoList_Features/delete-todo.html', context)
