from django.urls import path

from . import views

app_name = 'to-do-list'

urlpatterns = [
    path('', views.createToDo, name='create-todo'),
    path('edit-todo/<str:pk>/', views.editToDo, name='edit-todo'),
    path('delete-todo/<str:pk>/', views.deleteToDo, name='delete-todo'),
]
