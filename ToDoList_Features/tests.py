from django.test import TestCase, Client
from django.urls import resolve

from .models import ToDoList
from .views import createToDo, editToDo, deleteToDo
from .forms import ToDoForm

# Create your tests here.
class TestActivities(TestCase):

    # Test untuk fitur add To Do
    def test_url_create_todo(self):
        response = Client().get('/to-do-list/')
        self.assertEquals(200, response.status_code)

    def test_template_create_todo(self):
        response = Client().get('/to-do-list/')
        self.assertTemplateUsed(response, 'ToDoList_Features/create-todo.html')

    def test_view_func_create_todo(self):
        found = resolve('/to-do-list/')
        self.assertEqual(found.func, createToDo)

    def test_model_to_do_list(self):
        ToDoList.objects.create(todo = "Do TK1 PPW")
        count = ToDoList.objects.all().count()
        self.assertEqual(count, 1)
        test = ToDoList.objects.get(todo = "Do TK1 PPW")
        self.assertEqual(str(test), "Do TK1 PPW")

    def test_isi_dalam_to_do_list(self):
        form = ToDoForm()
        response = Client().get('/to-do-list/')
        isi_html = response.content.decode('utf8')

        self.assertIn("To Do List", isi_html)
        self.assertIn('placeholder="What should I do?"', form.as_p())

    # Test untuk fitur edit To Do
    def test_url_edit_todo(self):
        todos_count = ToDoList.objects.all().count()
        if (todos_count > 0):
            response = Client().get('/to-do-list/edit-todo/1/')
            self.assertEquals(200, response.status_code)

    def test_template_edit_todo(self):
        todos_count = ToDoList.objects.all().count()
        if (todos_count > 0):
            response = Client().get('/to-do-list/edit-todo/1/')
            self.assertTemplateUsed(response, 'ToDoList_Features/edit-todo.html')

    def test_view_func_edit_todo(self):
        todos_count = ToDoList.objects.all().count()
        if (todos_count > 0):
            found = resolve('/to-do-list/edit-todo/<str:pk>/')
            self.assertEqual(found.func, editToDo)

    # Test untuk fitur delete To Do
    def test_url_delete_todo(self):
        todos_count = ToDoList.objects.all().count()
        if (todos_count > 0):
            response = Client().get('/to-do-list/delete-todo/1/')

    def test_template_delete_todo(self):
        todos_count = ToDoList.objects.all().count()
        if (todos_count > 0):
            response = Client().get('/to-do-list/delete-todo/1/')
            self.assertTemplateUsed(response, 'ToDoList_Features/delete-todo.html')

    def test_view_func_delete_todo(self):
        todos_count = ToDoList.objects.all().count()
        if (todos_count > 0):
            found = resolve('/to-do-list/delete-todo/1/')
            self.assertEqual(found.func, deleteToDo)

    # Test untuk blank items maka tidak valid
    def test_form_validation_for_blank_items(self):
        form = ToDoForm(data={'todo': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['todo'],
            ["This field is required."]
        )
