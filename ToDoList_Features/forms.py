from django import forms
from django.forms import ModelForm
from .models import ToDoList

class ToDoForm(forms.ModelForm):
    class Meta:
        model = ToDoList
        fields = [
            'todo',
        ]

        labels = {
            'todo': "Insert your input below"
        }

        widgets = {
            'todo': forms.TextInput(attrs={'placeholder': 'What should I do?'}),
        }
